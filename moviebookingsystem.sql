-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2019 at 06:39 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moviebookingsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `movieId` int(11) NOT NULL,
  `movieName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`movieId`, `movieName`) VALUES
(1, 'Movie1'),
(2, 'Movie2');

-- --------------------------------------------------------

--
-- Table structure for table `moviestheatrescreenmapping`
--

CREATE TABLE `moviestheatrescreenmapping` (
  `id` int(11) NOT NULL,
  `movieId` int(11) NOT NULL,
  `theatreId` int(11) NOT NULL,
  `screenId` int(11) NOT NULL,
  `startTiming` datetime NOT NULL,
  `price` int(11) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `moviestheatrescreenmapping`
--

INSERT INTO `moviestheatrescreenmapping` (`id`, `movieId`, `theatreId`, `screenId`, `startTiming`, `price`, `duration`) VALUES
(1, 1, 1, 1, '2019-10-10 10:00:00', 120, 120),
(2, 2, 1, 1, '2019-11-10 12:00:00', 120, 120);

-- --------------------------------------------------------

--
-- Table structure for table `screens`
--

CREATE TABLE `screens` (
  `screenId` int(11) NOT NULL,
  `screenName` varchar(200) NOT NULL,
  `totalSeats` int(11) NOT NULL,
  `theatreId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `screens`
--

INSERT INTO `screens` (`screenId`, `screenName`, `totalSeats`, `theatreId`) VALUES
(1, 'Screen1', 120, 1),
(2, 'Screen2', 120, 1),
(3, 'Screen1', 100, 2),
(4, 'Screen2', 120, 2),
(5, 'Screen3', 120, 2),
(6, 'Screen3', 120, 1);

-- --------------------------------------------------------

--
-- Table structure for table `theatre`
--

CREATE TABLE `theatre` (
  `theatreId` int(11) NOT NULL,
  `theatreName` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `address` varchar(300) NOT NULL,
  `contact` bigint(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theatre`
--

INSERT INTO `theatre` (`theatreId`, `theatreName`, `location`, `address`, `contact`, `active`) VALUES
(1, 'Theatre1', 'Pune', 'ABC Address', 8696967854, 1),
(2, 'Theatre2', 'Pune', 'ABC Address', 9814758967, 1),
(3, 'Theatre 3', 'Pune', 'ABC Address', 7895466789, 1),
(4, 'Theatre 4', 'Pune', 'ABC Address', 7895466789, 1),
(5, 'Theatre 5', 'Pune', 'ABC Address', 7895466789, 1),
(6, 'Theatre 6', 'Pune', 'ABC Address', 7895466789, 1),
(7, 'Theatre 7', 'Pune', 'ABC Address', 7895466789, 1),
(8, 'Theatre 8', 'Pune', 'ABC Address', 7894457894, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `ticketId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `seatsBooked` int(11) NOT NULL,
  `mappingId` int(11) NOT NULL,
  `bookingDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`ticketId`, `userId`, `seatsBooked`, `mappingId`, `bookingDate`) VALUES
(1, 1, 2, 1, '2019-11-09 13:23:12'),
(2, 1, 4, 2, '2019-11-09 13:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `password` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userName`, `password`) VALUES
(1, 'User1', ''),
(2, 'User2', ''),
(3, 'User23', '%;2fS$U9O)@V7O\0:v5vLbU@bxw5La?[Xg,-B?gO''N*k^(1DEz}Q<\nMdH?"?:JiH?bM m-t]6?pi?l>frk?Ys)W51=>x3??JzE?8g?A(4hKSBY$7\n'),
(4, 'User14', '!5[MzzP-"$~@E@w90^?.U%:~*r\Z\07c?2(V5g9\\oA^cG:\\22}~ex j\ZI#IM1dEr{PBL%c_?i0#''~?!?\r)+w5iu^[ vdfMx5\nI?2\rsd_?B:Q:5s8}Yeq\0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`movieId`);

--
-- Indexes for table `moviestheatrescreenmapping`
--
ALTER TABLE `moviestheatrescreenmapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_moviescreenmmapping_movieId` (`movieId`),
  ADD KEY `fk_moviescreenmmapping_screenId` (`screenId`),
  ADD KEY `fk_moviestheatrescreenmapping_theatreId` (`theatreId`);

--
-- Indexes for table `screens`
--
ALTER TABLE `screens`
  ADD PRIMARY KEY (`screenId`),
  ADD KEY `fk_theatre_id` (`theatreId`);

--
-- Indexes for table `theatre`
--
ALTER TABLE `theatre`
  ADD PRIMARY KEY (`theatreId`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticketId`),
  ADD KEY `fk_tickets_userId` (`userId`),
  ADD KEY `fk_tickets_mappingid` (`mappingId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userName` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `movieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `moviestheatrescreenmapping`
--
ALTER TABLE `moviestheatrescreenmapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `screens`
--
ALTER TABLE `screens`
  MODIFY `screenId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `theatre`
--
ALTER TABLE `theatre`
  MODIFY `theatreId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticketId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `moviestheatrescreenmapping`
--
ALTER TABLE `moviestheatrescreenmapping`
  ADD CONSTRAINT `fk_moviescreenmmapping_movieId` FOREIGN KEY (`movieId`) REFERENCES `movies` (`movieId`),
  ADD CONSTRAINT `fk_moviescreenmmapping_screenId` FOREIGN KEY (`screenId`) REFERENCES `screens` (`screenId`),
  ADD CONSTRAINT `fk_moviestheatrescreenmapping_theatreId` FOREIGN KEY (`theatreId`) REFERENCES `theatre` (`theatreId`);

--
-- Constraints for table `screens`
--
ALTER TABLE `screens`
  ADD CONSTRAINT `fk_theatre_id` FOREIGN KEY (`theatreId`) REFERENCES `theatre` (`theatreId`);

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `fk_tickets_mappingid` FOREIGN KEY (`mappingId`) REFERENCES `moviestheatrescreenmapping` (`id`),
  ADD CONSTRAINT `fk_tickets_userId` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
