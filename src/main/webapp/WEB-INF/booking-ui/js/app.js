var app = angular.module('app', ['controllers','ngResource','ngRoute']);

app.config(['$routeProvider','$locationProvider',
    function($routeProvider,$locationProvider) {
        $routeProvider.
        when('/home', {
            templateUrl: 'booking-ui/views/partials/Home.html'
        }).
        when('/addTheatre', {
            templateUrl: 'booking-ui/views/partials/AddTheatre.html'
        }).
        when('/addScreen', {
            templateUrl: 'booking-ui/views/partials/AddScreen.html'
        }).
        when('/viewTheatres', {
            templateUrl: 'booking-ui/views/partials/ViewTheatres.html'
        }).
        otherwise({
            redirectTo: '/home'
        });
        $locationProvider.hashPrefix('');
    }]);