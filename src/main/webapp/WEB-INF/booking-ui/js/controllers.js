var controllers=angular.module("controllers",[]);


controllers.controller('HomeCtrl', ['$scope',   '$rootScope','$http',
	   function ($scope,  $rootScope,$http) {}]);

controllers.controller('TheatreCtrl', ['$scope',   '$rootScope','$http',
	   function ($scope,  $rootScope,$http) {
		$scope.showTheatres = false;
		$scope.theatreForm = false;
		$scope.screenForm = false;
		$scope.theatres={}
		$scope.theatresMap=[]
		$scope.theatreName=null;
		$scope.theatreLocation=null;
		$scope.theatreAddress=null;
		$scope.theatreContact=null;
		$scope.screens={}
    	$http.get("http://localhost:8080/moviebookingsystem/getTheatres")
    	  .then(function(response) {
    	    $scope.theatres = response.data;
    	  });
	    $scope.addTheatre=function()
	       {
	    		var data = {
	    				theatreName:$scope.theatreName,
	    				location:$scope.theatreLocation,
	    				address:$scope.theatreAddress,
	    				contact:$scope.theatreContact,
	    				};
	    	console.log(data);	
	    	console.log(JSON.stringify(data));
	    	$http.post("http://localhost:8080/moviebookingsystem/addOrUpdateTheatre", JSON.stringify(data)).then(function (response) {
	    		if (response.data)	
	    			alert("Theatre info saved successfully");
	    	}, function (response) {
	    		console.log(response);
	    	});
	       };
	}]);

controllers.controller('ScreenCtrl', ['$scope',   '$rootScope','$http',
	   function ($scope,  $rootScope,$http) {
		$scope.showTheatres = false;
		$scope.theatreForm = false;
		$scope.screenForm = false;
		$scope.theatres={}
		$scope.theatresMap=[]
		$scope.theatreName=null;
		$scope.theatreLocation=null;
		$scope.theatreAddress=null;
		$scope.theatreContact=null;
		$scope.screens={};
        $http.get("http://localhost:8080/moviebookingsystem/getTheatres")
    	  .then(function(response) {
    	    $scope.theatres = response.data;
    	    $scope.selectedTheatreId=$scope.theatres[0].theatreId;
    	  });
        $scope.addScreen=function()
       {
    	   var data = {
	    				screenName:$scope.screenName,
	    				totalSeats:$scope.totalSeats,
	    				theatreId:$scope.selectedTheatreId,
	    				};
	    	$http.post("http://localhost:8080/moviebookingsystem/addOrUpdateScreen", JSON.stringify(data)).then(function (response) {
	    		if (response.data)	
	    			alert("Screen info saved successfully");
	    	}, function (response) {
	    		console.log(response);
	    	});
	       };

	}]);