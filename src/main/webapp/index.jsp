<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 
pageEncoding="ISO-8859-1"%>
<!doctype html>
<html ng-app="app">
<head>        
    <title>Movie Booking System</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />	
 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="booking-ui/css/style.css"/>
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>        
 
 
    <script src="booking-ui/vendors/angular/angular.js"></script> 
    <script src="booking-ui/vendors/angular/angular-resource.min.js"></script> 
    <script src="booking-ui/vendors/angular/angular-route.min.js"></script>
    <script src="booking-ui/js/app.js"></script> 
    <script src="booking-ui/js/controllers.js"></script>
    <!--  <script src="resources/js/filters.js"></script>-->
    <!--  <script src="resources/js/services.js"></script>-->
    <!-- <script src="resources/js/directives.js"></script> -->
    
</head>
 
<body ng-controller="HomeCtrl">
	<div align="center" ng-include="'booking-ui/views/include/Header.html'" style="width:100%"></div>
		<div  ng-view class="view">
		</div>
	<div ng-include="'booking-ui/views/include/Footer.html'" style="width:100%"></div>
</body>
</html>