package com.moviebookingsystem.service;

import java.util.List;

import com.moviebookingsystem.beans.Screen;
import com.moviebookingsystem.beans.Theatre;
import com.moviebookingsystem.dao.TheatreDAOImpl;

public class TheatreServiceImpl implements TheatresService{
	TheatreDAOImpl theatreDAOObj=new TheatreDAOImpl();
	public List<Theatre> getTheatres(){
		
		List<Theatre> theatresList = theatreDAOObj.getTheatres();
		return theatresList;
	}
	public Boolean addOrUpdateTheatre(Theatre threatre) {
		Boolean result=theatreDAOObj.addOrUpdateTheatre(threatre);
		return result;
	}
	public Theatre getTheatreById(int theatreId) {
		Theatre theatre=theatreDAOObj.getTheatreById(theatreId);
		return theatre;
	}
	
	public Theatre getTheatresByMovie(int movieId) {
		Theatre theatre=theatreDAOObj.getTheatresByMovie(movieId);
		return theatre;
	}
	public Boolean deleteTheatre(int theatreId) {
		Boolean result=theatreDAOObj.deleteTheatre(theatreId);
		return result;
	}
	public Boolean addOrUpdateScreen(Screen screen) {
		Boolean result=theatreDAOObj.addOrUpdateScreen(screen);
		return result;
	} 
}
