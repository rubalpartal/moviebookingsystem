package com.moviebookingsystem.service;

import com.moviebookingsystem.beans.Ticket;
import com.moviebookingsystem.dao.TicketDAOImpl;

public class TicketsServiceImpl implements TicketsService{
	TicketDAOImpl ticketsDAOObj=new TicketDAOImpl();
	public Boolean saveTicketsInfo(Ticket ticket) {
		Boolean result=ticketsDAOObj.saveTicketsInfo(ticket);
		return result;
	}

}
