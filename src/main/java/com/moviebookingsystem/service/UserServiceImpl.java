package com.moviebookingsystem.service;

import java.util.List;

import com.moviebookingsystem.beans.BookingHistory;
import com.moviebookingsystem.beans.User;
import com.moviebookingsystem.dao.UserDAOImpl;

public class UserServiceImpl implements UserService{
	UserDAOImpl userDAOObj=new UserDAOImpl();
	public List<BookingHistory> getBookingHisotry(int userId){
		
		List<BookingHistory> bookingHistoryList = userDAOObj.getBookingHisotry(userId);
		return bookingHistoryList;
	}
	public Boolean registerUser(User user) {
		Boolean result=userDAOObj.registerUser(user);
		return result;
	}
	public User login(User user) {
		User result=userDAOObj.login(user);
		return result;
	}
}
