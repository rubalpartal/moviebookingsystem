package com.moviebookingsystem.service;

import java.util.List;

import com.moviebookingsystem.beans.Movie;
import com.moviebookingsystem.beans.MovieSlot;
import com.moviebookingsystem.beans.Theatre;
import com.moviebookingsystem.dao.MoviesDAOImpl;

public class MoviesServiceImpl {
	MoviesDAOImpl moviesDAOObj=new MoviesDAOImpl();
	public List<Movie> getMoviesByTheatre(int theatreId) {
		
		List<Movie> moviesList = moviesDAOObj.getMoviesByTheatre(theatreId);
		return moviesList;
	}
	public Boolean addMovie(Movie movie) {
		Boolean result=moviesDAOObj.addMovie(movie);
		return result;
	}
	public List<Movie> getAllMovies() {
		List<Movie> moviesList = moviesDAOObj.getAllMovies();
		return moviesList;
	}
	public Boolean addOrUpdateMovieSlot(MovieSlot movieSlot) {
		Boolean result=moviesDAOObj.addOrUpdateMovieSlot(movieSlot);
		return result;
	}
}
