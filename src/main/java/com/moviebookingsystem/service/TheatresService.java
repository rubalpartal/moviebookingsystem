package com.moviebookingsystem.service;

import java.util.List;

import com.moviebookingsystem.beans.Screen;
import com.moviebookingsystem.beans.Theatre;

public interface TheatresService {
	public List<Theatre> getTheatres();
	public Boolean addOrUpdateTheatre(Theatre threatre);
	public Theatre getTheatreById(int theatreId);
	public Theatre getTheatresByMovie(int movieId);
	public Boolean deleteTheatre(int theatreId);
	public Boolean addOrUpdateScreen(Screen screen);

}
