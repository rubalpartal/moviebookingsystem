package com.moviebookingsystem.service;

import java.util.List;

import com.moviebookingsystem.beans.Movie;
import com.moviebookingsystem.beans.MovieSlot;

public interface MoviesService {
	public List<Movie> getMoviesByTheatre(int theatreId);
	public Boolean addMovie(Movie movie);
	public List<Movie> getAllMovies();
	public Boolean addOrUpdateMovieSlot(MovieSlot movieSlot);
}
