package com.moviebookingsystem.beans;

import java.sql.Date;
import java.sql.Timestamp;

public class BookingHistory {
	private int ticketId;
	private int seatsBooked;
	private String movieName;
	private String screenName;
	private String theatreName;
	private String bookingDate;
	private int amount;
	private Timestamp movieTime;
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public int getSeatsBooked() {
		return seatsBooked;
	}
	public void setSeatsBooked(int seatsBooked) {
		this.seatsBooked = seatsBooked;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getTheatreName() {
		return theatreName;
	}
	public void setTheatreName(String theatreName) {
		this.theatreName = theatreName;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Timestamp getMovieTime() {
		return movieTime;
	}
	public void setMovieTime(Timestamp movieTime) {
		this.movieTime = movieTime;
	}

}
