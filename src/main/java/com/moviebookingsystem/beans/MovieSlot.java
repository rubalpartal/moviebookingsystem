package com.moviebookingsystem.beans;

import java.sql.Date;

public class MovieSlot {
	private int id;
	private int movieId;
	private int screenId;
	private int theatreId;
	private Date startTiming;
	private int price;
	private int duration;
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public int getScreenId() {
		return screenId;
	}
	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}
	public Date getStartTiming() {
		return startTiming;
	}
	public void setStartTiming(Date startTiming) {
		this.startTiming = startTiming;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getTheatreId() {
		return theatreId;
	}
	public void setTheatreId(int theatreId) {
		this.theatreId = theatreId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

}
