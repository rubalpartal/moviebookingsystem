package com.moviebookingsystem.beans;

public class Ticket {
	private int ticketId;
	private int userId;
	private int noOfTickets;
	private int movieScreenMappingId;
	private String bookingDate;
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getNoOfTickets() {
		return noOfTickets;
	}
	public void setNoOfTickets(int noOfTickets) {
		this.noOfTickets = noOfTickets;
	}
	public int getMovieScreenMappingId() {
		return movieScreenMappingId;
	}
	public void setMovieScreenMappingId(int movieScreenMappingId) {
		this.movieScreenMappingId = movieScreenMappingId;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	

}
