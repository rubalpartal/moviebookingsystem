package com.moviebookingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.moviebookingsystem.beans.Movie;
import com.moviebookingsystem.beans.MovieSlot;

public class MoviesDAOImpl implements MoviesDAO{
	public List<Movie> getMoviesByTheatre(int theatreId) {
		Connection connection = ConnectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM movies where movieId in(select movieId from moviestheatrescreenmapping where theatreId=" + theatreId + ")");
            List<Movie> moviesList=new ArrayList<Movie>();
            while(rs.next())
            {
            	Movie movies=new Movie();
                movies.setMovieName(rs.getString("movieName"));
                movies.setMovieId(rs.getInt("movieId"));
                moviesList.add(movies);
            }
            return moviesList;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return null;
	}
	public Boolean addMovie(Movie movie) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	        PreparedStatement ps = connection.prepareStatement("INSERT INTO movies(movieName) VALUES(?)");
	        ps.setString(1, movie.getMovieName());
	        int i = ps.executeUpdate();
	      if(i == 1) {
	        return true;
	      }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}
	
	public List<Movie> getAllMovies() {
		Connection connection = ConnectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM movies");
            List<Movie> moviesList=new ArrayList<Movie>();
            while(rs.next())
            {
            	Movie movies=new Movie();
                movies.setMovieName(rs.getString("movieName"));
                movies.setMovieId(rs.getInt("movieId"));
                moviesList.add(movies);
            }
            return moviesList;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return null;
	}
	public Boolean addOrUpdateMovieSlot(MovieSlot movieSlot) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	    	PreparedStatement ps=null;
	    	if(movieSlot.getId()==0) {
	    		ps = connection.prepareStatement("INSERT INTO moviestheatrescreenmapping(movieId,theatreId,screenId,price,duration) VALUES(?,?,?,?,?)");
	    	}
	    	else
	    	{
	    		ps= connection.prepareStatement("UPDATE moviestheatrescreenmapping SET movieId=?, theatreId=?, screenId=?,price=?,duration=?,startTiming=? WHERE screenId="+movieSlot.getId());
	    	}
	        ps.setInt(1, movieSlot.getMovieId());
	        ps.setInt(2, movieSlot.getTheatreId());
	        ps.setInt(3, movieSlot.getScreenId());
	        ps.setInt(4, movieSlot.getPrice());
	        ps.setInt(5, movieSlot.getDuration());
	        ps.setDate(6, movieSlot.getStartTiming());
	        
	        int i = ps.executeUpdate();
	      if(i == 1) {
	        return true;
	      }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}
	
}
