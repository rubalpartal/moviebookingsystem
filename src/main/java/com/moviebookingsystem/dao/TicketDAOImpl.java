package com.moviebookingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.moviebookingsystem.beans.Ticket;

public class TicketDAOImpl implements TicketsDAO {
	public Boolean saveTicketsInfo(Ticket ticket) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	        PreparedStatement ps = connection.prepareStatement("INSERT INTO tickets(userId,seatsBooked,mappingId) VALUES (?, ?, ?)");
	        ps.setInt(1, ticket.getUserId());
	        ps.setInt(2, ticket.getNoOfTickets());
	        ps.setInt(3, ticket.getMovieScreenMappingId());
	        int i = ps.executeUpdate();
	      if(i == 1) {
	        return true;
	      }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}

}
