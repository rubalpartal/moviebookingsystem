package com.moviebookingsystem.dao;

import java.util.List;

import com.moviebookingsystem.beans.Movie;
import com.moviebookingsystem.beans.MovieSlot;

public interface MoviesDAO {
	public List<Movie> getMoviesByTheatre(int theatreId);
	public Boolean addMovie(Movie movie);
	public List<Movie> getAllMovies();
	public Boolean addOrUpdateMovieSlot(MovieSlot movieSlot);
}
