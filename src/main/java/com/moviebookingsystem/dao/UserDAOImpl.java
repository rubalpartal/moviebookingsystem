package com.moviebookingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.moviebookingsystem.beans.BookingHistory;
import com.moviebookingsystem.beans.Theatre;
import com.moviebookingsystem.beans.User;

public class UserDAOImpl implements UserDAO{
	public List<BookingHistory> getBookingHisotry(int userId){
		Connection connection = ConnectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ticketId,seatsBooked,movieName,theatreName,screenName,price,startTiming FROM tickets t inner join moviestheatrescreenmapping m on m.id=t.mappingId "
            		+ "INNER JOIN movies on movies.movieId=m.movieId INNER JOIN "
            		+ "theatre on theatre.theatreId=m.theatreId "
            		+ "INNER JOIN screens on screens.screenId=m.screenId where t.userId="+userId);
            List<BookingHistory> bookingHistoryList=new ArrayList<BookingHistory>();
            while(rs.next())
            {
            	BookingHistory bookingInfo=new BookingHistory();
            	bookingInfo.setTicketId(rs.getInt(1));
            	bookingInfo.setSeatsBooked(rs.getInt(2));
            	bookingInfo.setMovieName(rs.getString(3));
            	bookingInfo.setTheatreName(rs.getString(4));
            	bookingInfo.setScreenName(rs.getString(5));
            	bookingInfo.setAmount(rs.getInt(6)*rs.getInt(2));
            	Timestamp movieTime = rs.getTimestamp(7) ;
            	bookingInfo.setMovieTime(movieTime);
            	bookingHistoryList.add(bookingInfo);
            }
            return bookingHistoryList;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return null;
	}
	
	public Boolean registerUser(User user) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	    	PreparedStatement	ps = connection.prepareStatement("INSERT INTO users(userName,password) VALUES (?, ?)");
	        ps.setString(1, user.getUserName());
	        ps.setString(2, user.getPassword());
	        int i = ps.executeUpdate();
	      if(i == 1) {
	        return true;
	      }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}
	
	public User login(User user) {
		Connection connection = ConnectionFactory.getConnection();
		User usr=new User();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT password FROM users where userName='"+ user.getUserName()+ "'");
            
            if(rs.next())
            {
            	usr.setPassword(rs.getString("password"));
            }
            
            return usr;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
	    return usr;
	}
	
}
