package com.moviebookingsystem.dao;

import java.util.List;

import com.moviebookingsystem.beans.BookingHistory;
import com.moviebookingsystem.beans.User;

public interface UserDAO {
	public List<BookingHistory> getBookingHisotry(int userId);
	public Boolean registerUser(User user);
	public User login(User user);

}
