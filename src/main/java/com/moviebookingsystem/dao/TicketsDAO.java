package com.moviebookingsystem.dao;

import com.moviebookingsystem.beans.Ticket;

public interface TicketsDAO {
	public Boolean saveTicketsInfo(Ticket ticket);
}
