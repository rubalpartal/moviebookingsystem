package com.moviebookingsystem.dao;

import java.util.List;

import com.moviebookingsystem.beans.Screen;
import com.moviebookingsystem.beans.Theatre;

public interface TheatreDAO {
	public List<Theatre> getTheatres();
	public Boolean addOrUpdateTheatre(Theatre theatre);
	public Theatre getTheatreById(int theatreId);
	public Theatre getTheatresByMovie(int movieId);
	public Boolean addOrUpdateScreen(Screen screen);
	public Boolean deleteTheatre(int theatreId);
}
