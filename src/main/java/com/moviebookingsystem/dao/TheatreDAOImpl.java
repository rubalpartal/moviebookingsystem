package com.moviebookingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.moviebookingsystem.beans.Screen;
import com.moviebookingsystem.beans.Theatre;

public class TheatreDAOImpl implements TheatreDAO{
	public List<Theatre> getTheatres(){
		Connection connection = ConnectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            Statement stmt2 = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM theatre where active=1");
            List<Theatre> theatreList=new ArrayList<Theatre>();
            while(rs.next())
            {
            	Theatre theatre=new Theatre();
            	theatre.setTheatreName(rs.getString("theatreName"));
            	theatre.setTheatreId(rs.getInt("theatreId"));
            	theatre.setAddress(rs.getString("address"));
            	theatre.setContact(rs.getLong("contact"));
            	theatre.setLocation(rs.getString("location"));
            	ResultSet screenResultSet = stmt2.executeQuery("SELECT * FROM screens where theatreId=" + theatre.getTheatreId());
            	List<Screen> screensList =new ArrayList<Screen>();
            	while(screenResultSet.next()) {
            		Screen screen=new Screen();
            		screen.setScreenId(screenResultSet.getInt("screenId"));
            		screen.setScreenName(screenResultSet.getString("screenName"));
            		screensList.add(screen);
            	}
            	theatre.setScreens(screensList);
            	theatreList.add(theatre);
            }
            return theatreList;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return null;
	}
	public Boolean addOrUpdateTheatre(Theatre theatre) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	    	
	    	PreparedStatement ps=null;
	    	if(theatre.getTheatreId()==0) {
	    		ps = connection.prepareStatement("INSERT INTO theatre(theatreName,location,address,contact) VALUES (?, ?, ?,?)");
	    	}
	    	else
	    	{
	    		ps = connection.prepareStatement("UPDATE theatre SET theatrename=?, location=?, address=?,contact=? WHERE theatreid="+theatre.getTheatreId());
	    	}
	        ps.setString(1, theatre.getTheatreName());
	        ps.setString(2, theatre.getLocation());
	        ps.setString(3, theatre.getAddress());
	        ps.setLong(4, theatre.getContact());
	        int i = ps.executeUpdate();
	      if(i == 1) {
	        return true;
	      }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}
	
	public Boolean addOrUpdateScreen(Screen screen) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	    	PreparedStatement ps=null;
	    	if(screen.getScreenId()==0) {
	    		ps = connection.prepareStatement("INSERT INTO screens(screenName,totalSeats,theatreId) VALUES (?, ?, ?)");
	    	}
	    	else
	    	{
	    		ps= connection.prepareStatement("UPDATE screens SET screenName=?, totalSeats=?, theatreId=? WHERE screenId="+screen.getScreenId());
	    	}
	        ps.setString(1,screen.getScreenName());
	        ps.setInt(2, screen.getTotalSeats());
	        ps.setInt(3, screen.getTheatreId());
	        int i = ps.executeUpdate();
	      if(i == 1) {
	        return true;
	      }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}
	
	public Theatre getTheatreById(int theatreId){
		Connection connection = ConnectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM theatre where theatreId="+ theatreId);
            Theatre theatre=new Theatre();
            if(rs.next())
            {
            	theatre.setTheatreName(rs.getString("theatreName"));
            	theatre.setTheatreId(rs.getInt("theatreId"));
            	theatre.setAddress(rs.getString("address"));
            	theatre.setContact(rs.getLong("contact"));
            	theatre.setLocation(rs.getString("location"));
            }
            return theatre;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return null;
	}
	
	public Theatre getTheatresByMovie(int movieId){
		Connection connection = ConnectionFactory.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM theatre where theatreId in( select theatreid from movietheatrescreenmapping"
            		+ "where movieId="+ movieId + ") and active=1");
            Theatre theatre=new Theatre();
            if(rs.next())
            {
            	theatre.setTheatreName(rs.getString("theatreName"));
            	theatre.setTheatreId(rs.getInt("theatreId"));
            	theatre.setAddress(rs.getString("address"));
            	theatre.setContact(rs.getLong("contact"));
            	theatre.setLocation(rs.getString("location"));
            }
            return theatre;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return null;
	}
	
	public Boolean deleteTheatre(int theatreId) {
		Connection connection = ConnectionFactory.getConnection();
	    try {
	    	Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM tickets where mappingId in( SELECT id FROM moviestheatrescreenmapping where theatreId="+ theatreId + " and DATEDIFF(date(startTiming),curdate())>=1)");
            if(rs.next()) {
            	return false;
            }
	        PreparedStatement ps = connection.prepareStatement("UPDATE theatre SET active=? WHERE theatreid=?");
	        ps.setInt(1, 0);
	        ps.setInt(2, theatreId);
	        int i = ps.executeUpdate();
	        if(i == 1) {
	        	return true;
	        }
	    } catch (SQLException ex) {
	        ex.printStackTrace();
	    }
	    return false;
	}
}
