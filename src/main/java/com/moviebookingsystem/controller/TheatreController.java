package com.moviebookingsystem.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.moviebookingsystem.beans.Screen;
import com.moviebookingsystem.beans.Theatre;
import com.moviebookingsystem.service.TheatreServiceImpl;

@RestController
public class TheatreController {
	
	TheatreServiceImpl serviceObj=new TheatreServiceImpl();
	@RequestMapping(value="/getTheatres", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody List<Theatre> getTheatres() 
	{
		List<Theatre> theatresList=serviceObj.getTheatres();
		return theatresList;
	}
	@RequestMapping(value="/addOrUpdateTheatre",method = RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody Boolean addOrUpdateTheatre(@RequestBody Theatre theatre) 
	{
		Boolean result=serviceObj.addOrUpdateTheatre(theatre);
		return result;
	}
	
	@RequestMapping(value="/getTheatreById",produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody Theatre getTheatreById(@RequestBody int theatreId) 
	{
		Theatre theatre=serviceObj.getTheatreById(theatreId);
		return theatre;
	}
	
	@RequestMapping(value="/getTheatresByMovie",produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody Theatre getTheatresByMovie(@RequestBody int movieid) 
	{
		Theatre theatre=serviceObj.getTheatresByMovie(movieid);
		return theatre;
	}
	
	@RequestMapping("/deleteTheatre")
	public Boolean deleteTheatre(@RequestParam int theatreId) 
	{
		
		Boolean result=serviceObj.deleteTheatre(theatreId);
		return result;
	}
	
	@RequestMapping(value="/addOrUpdateScreen",method = RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody Boolean addOrUpdateScreen(@RequestBody Screen screen) 
	{
		Boolean result=serviceObj.addOrUpdateScreen(screen);
		return result;
	}
	

}
