package com.moviebookingsystem.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.moviebookingsystem.beans.Ticket;
import com.moviebookingsystem.service.TicketsServiceImpl;

@Controller
public class TicketController {
	TicketsServiceImpl serviceObj=new TicketsServiceImpl();
	
	@RequestMapping(value="/saveTicketsInfo",method = RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody Boolean saveTicketsInfo(@RequestBody Ticket ticket) 
	{
		
		Boolean result=serviceObj.saveTicketsInfo(ticket);
		return result;
	}

}
