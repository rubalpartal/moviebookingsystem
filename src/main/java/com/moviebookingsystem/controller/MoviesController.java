package com.moviebookingsystem.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.moviebookingsystem.beans.Movie;
import com.moviebookingsystem.beans.MovieSlot;
import com.moviebookingsystem.service.MoviesServiceImpl;

@Controller
public class MoviesController {
	MoviesServiceImpl serviceObj=new MoviesServiceImpl();
	@RequestMapping(value="/getMoviesByTheatre",produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody List<Movie> getMoviesByTheatre(@RequestBody int theatreId) 
	{
		
		List<Movie> moviesList=serviceObj.getMoviesByTheatre(theatreId);
		return moviesList;
	}
	
	@RequestMapping(value="/addMovie",method = RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody Boolean addMovie(@RequestBody Movie movie) 
	{
		Boolean result=serviceObj.addMovie(movie);
		return result;
	}
	
	@RequestMapping(value="/addOrUpdateMovieSlot",method = RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public @ResponseBody Boolean addOrUpdateMovieSlot(@RequestBody MovieSlot movieSlot) 
	{
		Boolean result=serviceObj.addOrUpdateMovieSlot(movieSlot);
		return result;
	}
	
	
}