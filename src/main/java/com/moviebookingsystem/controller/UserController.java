package com.moviebookingsystem.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moviebookingsystem.beans.BookingHistory;
import com.moviebookingsystem.service.UserServiceImpl;

@Controller
public class UserController {
	UserServiceImpl serviceObj=new UserServiceImpl();	
	@RequestMapping(value="/getBookingHistory",produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody List<BookingHistory> getBookingHistory(@RequestBody int userId) 
	{
		
		List<BookingHistory> bookingHistoryList=serviceObj.getBookingHisotry(userId);
		return bookingHistoryList;
	}
	
}
